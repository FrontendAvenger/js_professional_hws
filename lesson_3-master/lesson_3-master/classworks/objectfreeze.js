/*
  Задание: написать функцию, для глубокой заморозки обьектов.

  Обьект для работы:
  let universe = {
    infinity: Infinity,
    good: ['cats', 'love', 'rock-n-roll'],
    evil: {
      bonuses: ['cookies', 'great look']
    }
  };

  frozenUniverse.evil.humans = []; -> Не должен отработать.

  Методы для работы:
  1. Object.getOwnPropertyNames(obj);
      -> Получаем имена свойств из объекта obj в виде массива

  2. Проверка через typeof на обьект или !== null
  if (typeof prop == 'object' && prop !== null){...}

  Тестирование:

  let FarGalaxy = DeepFreeze(universe);
      FarGalaxy.good.push('javascript'); // false
      FarGalaxy.something = 'Wow!'; // false
      FarGalaxy.evil.humans = [];   // false

*/

let universe = {
    infinity: Infinity,
    good: ['cats', 'love', 'rock-n-roll'],
    evil: {
      bonuses: ['cookies', 'great look']
    },
    obj: {
      obj: {
        obj: {
          obj : {
            cat: 'meow'
          }
        }
      }
    },
    obj_arr: [{}, {Golum:'My Precious'}],
  };



const DeepFreeze = ( obj ) => {
  if(typeof obj === 'object' && obj !== null){

      for( let key in obj){
          DeepFreeze( obj[key] );
      }
      console.log('property was frozen:', obj)
      return Object.freeze( obj ); 
  } else {
    console.warn('sorry wrong object type', typeof obj);
  }
}

const work = () => {
  console.log( 'works' );

    let FarGalaxy = DeepFreeze(universe);
      // FarGalaxy.good.push('javascript'); // false
      // FarGalaxy.something = 'Wow!'; // false
      // FarGalaxy.evil.humans = [];   // false
      // FarGalaxy.obj.obj.obj.obj.cat = 'woof';
      FarGalaxy.obj_arr[1].Golum = 'Your Precious';

      console.log( 'FarGalaxy', FarGalaxy )
}

export default work;
