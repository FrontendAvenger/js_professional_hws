  /*
    import defaultExport from "module-name"; <- node_modules
    import * as name from "module-name";
    import { export } from "module-name";
    import { export as alias } from "module-name";
    import { export1 , export2 } from "module-name";
    import { export1 , export2 as alias2 } from "module-name";
    import defaultExport, * as name from "module-name";
    import "module-name";
  */
  /*
    import defaultExport from "module-name";
  */

import Government from '../classworks/singleton';

import objectFreeze from '../classworks/objectfreeze';

objectFreeze(); 