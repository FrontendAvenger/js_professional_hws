import Government from "./singleton"

const Singleton = () => {
  Government.addLaw(1,"law 1","Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste labore id quisquam corrupti."); 
  Government.readConstitutuon();
  Government.readLaw(1); 
  Government.showcitizensSatisfactions();  
  Government.showBudjet();
  Government.organizeHol();
}

export default Singleton;