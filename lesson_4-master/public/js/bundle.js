/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classwork_task2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../classwork/task2 */ \"./classwork/task2.js\");\n\nObject(_classwork_task2__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./classwork/task1.js":
/*!****************************!*\
  !*** ./classwork/task1.js ***!
  \****************************/
/*! exports provided: BackendDeveloper, FrontendDeveloper, Designer, ProjectManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"BackendDeveloper\", function() { return BackendDeveloper; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"FrontendDeveloper\", function() { return FrontendDeveloper; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Designer\", function() { return Designer; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ProjectManager\", function() { return ProjectManager; });\n/*\n  Composition:\n\n  Задание при помощи композиции создать объекты 4х типов:\n\n  functions:\n    - MakeBackendMagic\n    - MakeFrontendMagic\n    - MakeItLooksBeautiful\n    - DistributeTasks\n    - DrinkSomeTea\n    - WatchYoutube\n    - Procrastinate\n\n  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate\n  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube\n  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate\n  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea\n\n  ProjectManager(name,gender,age) => { name, gender, age, type: 'project'}\n\n*/\n\n\n\n  const MakeBackendMagic = ( person ) => ({\n    makebackendmagic: () => console.log('MakeBackendMagic wowowowo' )\n  });\n\n  const DrinkSomeTea = ( person ) => ({\n    drinkSomeTea: () => console.log('DrinkSomeTea wowowowo' )\n  });\n\n  const Procrastinate = ( person ) => ({\n    procrastinate: () => console.log('Procrastinate wowowowo' )\n  });\n\n  const MakeFrontendMagic = ( person ) => ({\n    makeFrontendMagic: () => console.log('MakeFrontendMagic wowowowo' )\n  });\n\n  const WatchYoutube = ( person ) => ({\n    watchYoutube: () => console.log('WatchYoutube wowowowo' )\n  });\n\n  const MakeItLooksBeautiful = ( person ) => ({\n    makeItLooksBeautiful: () => console.log('MakeItLooksBeautiful wowowowo' )\n  });\n\n  const DistributeTasks = ( person ) => ({\n    distributeTasks: () => console.log('DistributeTasks wowowowo' )\n  });\n\n\n  const BackendDeveloper = (name, gender, age) => {\n    let person = {\n      name,\n      gender,\n      age,\n      type : \"backend\"\n    };\n\n    return Object.assign(\n      {},\n      person,\n      MakeBackendMagic(person),\n      DrinkSomeTea(person),\n      Procrastinate(person)\n    );\n  };\n\n  const FrontendDeveloper = (name, gender, age) => {\n    let person = {\n      name,\n      gender,\n      age,\n      type : \"frontend\"\n    };\n\n    return Object.assign(\n      {},\n      person,\n      MakeFrontendMagic(person),\n      DrinkSomeTea(person),\n      WatchYoutube(person)\n    );\n  };\n\n  const Designer = (name, gender, age) => {\n    let person = {\n      name,\n      gender,\n      age,\n      type : \"design\"\n    };\n\n    return Object.assign(\n      {},\n      person,\n      Procrastinate(person),\n      MakeItLooksBeautiful(person),\n      WatchYoutube(person)\n    );\n  };\n\n  const ProjectManager = (name, gender, age) => {\n    let person = {\n      name,\n      gender,\n      age,\n      type : \"project\"\n    };\n\n    return Object.assign(\n      {},\n      person,\n      Procrastinate(person),\n      DrinkSomeTea(person),\n      DistributeTasks(person)\n    );\n  };\n\n\n  \n\n\n\n\n\n\n//# sourceURL=webpack:///./classwork/task1.js?");

/***/ }),

/***/ "./classwork/task2.js":
/*!****************************!*\
  !*** ./classwork/task2.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _task1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./task1 */ \"./classwork/task1.js\");\n/*\n\n  Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет\n  расспределять и создавать сотрудников компании нужного типа.\n\n  Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2\n\n  HeadHunt => {\n    hire( obj ){\n      ...\n    }\n  }\n\n  Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики\n\n*/\n\n\n\nclass HeadHunt {\n\thire (employee){\n\t\tlet newObj;\n\t\tif (employee.type === 'backend'){\n\t\t\tnewObj = _task1__WEBPACK_IMPORTED_MODULE_0__[\"BackendDeveloper\"];\n\t\t}\n\t\telse if (employee.type === 'frontend'){\n\t\t\tnewObj = _task1__WEBPACK_IMPORTED_MODULE_0__[\"FrontendDeveloper\"];\n\t\t}\n\t\telse if (employee.type === 'design'){\n\t\t\tnewObj = _task1__WEBPACK_IMPORTED_MODULE_0__[\"Designer\"];\n\t\t}\n\t\telse if (employee.type === 'project'){\n\t\t\tnewObj = _task1__WEBPACK_IMPORTED_MODULE_0__[\"ProjectManager\"];\n\t\t}\n\t\telse{\n\t\t\treturn false;\n\t\t}\n\n\t\treturn  newObj( employee.name, employee.gender, employee.age);\n\t}\n}\n\nconst Fabric = new HeadHunt();\n\nlet employeeDiv = document.getElementById('employee');\nlet hireDiv = document.getElementById('hire');\nlet comandDiv = document.getElementById('command');\n\n\nconst GetEmployee = async () => {\n\tconst res = await fetch('http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2');\n\tconst data = await res.json();\n\t\n\tdata.forEach( (item) => {\n\t\tlet node = document.createElement('div');\n\t\t\temployeeDiv.appendChild(node);\n\n\t\tlet text = document.createElement('p');\n\t\t\ttext.innerHTML = `${item.name}  (${item.type})`;\n\t\t\tnode.appendChild(text);\n\n\t\tlet btn = document.createElement('button');\n\t\t\tbtn.innerHTML = 'Нанять';\n\t\t\thireDiv.appendChild(btn);\n\n\t\tbtn.addEventListener('click', () => {\n\t\t\tlet Employee = Fabric.hire( item );\n\t\t\tconsole.log(Employee);\n\t\t\tlet li = document.createElement('li');\n\t\t\t\tli.innerHTML = `${Employee.name} (${Employee.type})`;\n\t\t\t\tcomandDiv.appendChild(li);\n\t\t})\n\t})\n\n\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (GetEmployee);\n\n\n//# sourceURL=webpack:///./classwork/task2.js?");

/***/ })

/******/ });