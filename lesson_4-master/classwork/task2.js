/*

  Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет
  расспределять и создавать сотрудников компании нужного типа.

  Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2

  HeadHunt => {
    hire( obj ){
      ...
    }
  }

  Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики

*/

import { BackendDeveloper, FrontendDeveloper, Designer, ProjectManager } from './task1'

class HeadHunt {
	hire (employee){
		let newObj;
		if (employee.type === 'backend'){
			newObj = BackendDeveloper;
		}
		else if (employee.type === 'frontend'){
			newObj = FrontendDeveloper;
		}
		else if (employee.type === 'design'){
			newObj = Designer;
		}
		else if (employee.type === 'project'){
			newObj = ProjectManager;
		}
		else{
			return false;
		}

		return  newObj( employee.name, employee.gender, employee.age);
	}
}

const Fabric = new HeadHunt();
let employeeDiv = document.getElementById('employee');
let hireDiv = document.getElementById('hire');
let comandDiv = document.getElementById('command');


const GetEmployee = async () => {
	const res = await fetch('http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2');
	const data = await res.json();
	data.forEach( (item) => {
		let node = document.createElement('div');
		employeeDiv.appendChild(node);
		let text = document.createElement('p');
		text.innerHTML = `${item.name}  (${item.type})`;
		node.appendChild(text);
		let btn = document.createElement('button');
		btn.innerHTML = 'Нанять';
		hireDiv.appendChild(btn);
		btn.addEventListener('click', () => {
		let Employee = Fabric.hire( item );
		console.log(Employee);
		let li = document.createElement('li');
		li.innerHTML = `${Employee.name} ${Employee.gender} ${Employee.age} ${Employee.balance} (${Employee.type})`;
		comandDiv.appendChild(li);
		})
	})


}

export default GetEmployee;
