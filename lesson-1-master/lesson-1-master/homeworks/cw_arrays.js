/*
  Задание:
  1. При помощи методов изложеных в arrays.js , переформатировать ITEA_COURSES в массив который содержит длину строк каждого из элементов.
  2. Самостоятельно изучить метод Array.sort. Отфильтровать массив ITEA_COURSES по алфавиту.
      + Бонусный бал. Вывести на страничку списком
  3. Реализация функции поиска по массиву ITEA_COURSES.
      + Бонусный бал. Вывести на страничку инпут и кнопку по которой будет срабатывать поиск.

*/

const ITEA_COURSES = ["Курс HTML & CSS", "JavaScript базовый курс", "JavaScript продвинутый курс", "JavaScript Professional", "Angular 2.4 (базовый)", "Angular 2.4 (продвинутый)", "React.js", "React Native", "Node.js", "Vue.js"];

const num = item => console.log(item, '- '+item.length );
ITEA_COURSES.forEach( num );

console.log (' - - - - - - - - -');

const sorted = ITEA_COURSES.sort();
const sorted_list = item => {
	let List = document.getElementById('List')
	let li = document.createElement("li")
	List.appendChild(li);
	li.innerHTML = item;
}
sorted.forEach(sorted_list);
console.log(sorted);

