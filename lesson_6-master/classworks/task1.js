/*

  Задание: используя паттерн декоратор, модифицировать класс Human из примера basicUsage.

  0.  Создать новый конструктор, который будет принимать в себя человека как аргумент,
      и будем добавлять ему массив обьектов coolers (охладители), а него внести обьекты
      например мороженное, вода, сок и т.д в виде: {name: 'icecream', temperatureCoolRate: -5}

  1.  Расширить обработку функции ChangeTemperature в прототипе human таким образом,
      что если темпаретура становится выше 30 градусов то мы берем обьект из массива coolers
      и "охлаждаем" человека на ту температуру которая там указана.

      Обработку старого события если температура уходит вниз поставить с условием, что температура ниже нуля.
      Если температура превышает 50 градусов, выводить сообщение error -> "{Human.name} зажарился на солнце :("

  2.  Бонус: добавить в наш прототип нашего нового класса метод .addCooler(), который
      будет добавлять "охладитель" в наш обьект. Сделать валидацию что бы через этот метод
      нельзя было прокинуть обьект в котором отсутствует поля name и temperatureCoolRate.
      Выводить сообщение с ошибкой про это.

*/

class Human {
    constructor(name){
      this.name = name;
      this.currentTemperature = 15;
      this.minTemperature = -10;
      this.maxDangerTemperature = 50;
      this.maxTemperature = 30;

      console.log(`new Human ${this.name} arrived!`, this);
    }

    changeTemperature(changeValue){
      console.log(
        'current', this.currentTemperature + changeValue);

      this.currentTemperature = this.currentTemperature + changeValue;

      if( this.currentTemperature < this.minTemperature ){
        console.error(`Temperature is to low: ${this.currentTemperature}. ${this.name} died :(`);
      } 
      else if (this.currentTemperature > this.maxDangerTemperature) {
        console.error(`${this.name} зажарился на солнце :(`);
      }
      else if (this.currentTemperature > this.maxTemperature){
        console.log('Температура выше 30!!!');

        if (this.coolers.length > 0){
          let randomCooler = this.coolers[Math.floor(Math.random()*this.coolers.length)];
          this.currentTemperature = this.currentTemperature + randomCooler.temperatureCoolRate;
          
          this.coolers.splice(this.coolers.indexOf(randomCooler),1);
          console.log(`Температура сейчас после ${randomCooler.name} `, this.currentTemperature);
        } else {
          console.error('Закончились охладители ((');
        }
      }
      else {
        console.log('Температура в норме');
      }
    }
  }

   class HumanCold extends Human{
    constructor(name){
      super(name);
      this.coolers = [
        {name: 'ice cream', temperatureCoolRate: -5},
        {name: 'cold water', temperatureCoolRate: -10},
        {name: 'ice', temperatureCoolRate: -15},
        {name: 'cold juice', temperatureCoolRate: -7}
      ];
    }
  }

const BeachParty = () => {
  let John = new HumanCold('John');
      John.changeTemperature(5);
      John.changeTemperature(15);
      John.changeTemperature(15);
}

export default BeachParty;
